#include <string>
#include <iostream>
#include <random>


using std::string;

string randDNA(int seed, string bases, int n)
{

	string base;
	std::mt19937 seq1(seed);
	std::uniform_int_distribution<int> dist(0, bases.size() -1);
	
	for (int i=0; i<n; i++)
		{
			base += bases[dist(seq1)];
		}
		
return base;
}
